import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
const vuetify = new Vuetify()
Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
